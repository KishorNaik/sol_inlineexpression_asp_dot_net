﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Sol_Inline_Expression.App_Code
{
    public class PersonDal
    {
       

        public IEnumerable<Person> GetPersonData()
        {
            return
                    new List<Person>()
                    {
                        new Person()
                        {
                            FirstName="Kishor",
                            LastName="Naik"
                        },
                        new Person()
                        {
                            FirstName="Eshaan",
                            LastName="Naik"
                        }
                    };
        }
    }
}