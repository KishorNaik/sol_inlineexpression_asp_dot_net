﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayExpression.aspx.cs" Inherits="Sol_Inline_Expression.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
        <%--Displaying Expression--%>
        <div>
            <%= Server.HtmlEncode(DateTime.Now.ToString()) %>
        </div>

        <%--Html Encoding--%>

        <div>
            <%:Server.HtmlEncode(DateTime.Now.ToString()) %>
        </div>

    </form>
</body>
</html>
