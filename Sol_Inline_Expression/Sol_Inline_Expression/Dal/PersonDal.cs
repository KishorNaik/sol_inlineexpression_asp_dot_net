﻿using Sol_Inline_Expression.Dal.ORD;
using Sol_Inline_Expression.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression.Dal
{
    public class PersonDal
    {
        public IEnumerable<PersonEntity> GetPersonData()
        {

            //return
            //        new List<PersonEntity>()
            //        {
            //            new PersonEntity()
            //            {
            //                FirstName="Kishor",
            //                LastName="Naik"
            //            }
            //        };

            PersonDcDataContext dc = new PersonDcDataContext();


            return dc
            ?.Persons
            ?.AsEnumerable()
            ?.Select((letblPersonObj) => new PersonEntity()
            {

                FirstName = letblPersonObj.FirstName,
                LastName = letblPersonObj.LastName

            })
            .ToList();

        }
    }
}