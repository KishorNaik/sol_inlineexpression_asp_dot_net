﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InlineCodeBlockExpression.aspx.cs" Inherits="Sol_Inline_Expression.InlineCodeBlockExpression" %>
<%@ Import Namespace="Sol_Inline_Expression.Dal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        .lableDiv
        {
            color:white;
            position:relative;
            text-align:center;
            margin:auto;
            top: 8px;
            left: 3px;
            height: 27px;
            width: 100px;
            /*border-radius:5px;
            border:2px solid red;*/
            padding-right:10px;
        }

        .divStyle{
            
            height:150px;
            width:150px;
            margin:15px;
            border:2px;
            border-color:red;
            border-style:solid;
            border-radius:150px;
            padding:15px;
            float:left;
            overflow:hidden;
            position:relative;
        }

        /*.div-circle{
            background-color:white;
            border-color:gray;
            border-style:solid;
            border-radius:10px;
            height:100px;
            width:100px;
           
        }*/

        .div-inner{
            background-color:rgb(36, 35, 35);
            height:50px;
            width:170px;
            position:relative;
            top: 118px;
            left: -8px;
            opacity:0.5;
             border-radius:1px;
            /*border:2px solid red;*/
        }

        .img-center{
            height:120px;
            width:120px;
            position:absolute;
            top:10px;
            left:30px;
        }
    </style>

</head>

<body>
    <form id="form1" runat="server">
    <div>
        
          <%--  Code Block Expression--%>

            <%
                var personListResultSet = new PersonDal().GetPersonData();
                 %>

            <%
                foreach (var obj in personListResultSet)
                {

                    string concatFullName = new StringBuilder()
                                                            .Append(obj.FirstName)
                                                            .Append(" ")
                                                            .Append(obj.LastName)
                                                            .ToString();

                 %>
                        <div class="divStyle">

                            <img src="https://s-media-cache-ak0.pinimg.com/originals/9d/86/bc/9d86bcfa9b575914807469a50a7a36fd.jpg" class="img-center" />

                            <div class="div-inner">
                                <div class="lableDiv"><% Response.Write(Server.HtmlEncode(concatFullName)); %></div>
                            </div>
                           
                        </div>
                <%
                    }
                     %>
    </div>
    </form>
</body>
</html>
