﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeBehindInlineCode.aspx.cs" Inherits="Sol_Inline_Expression.CodeBehindInlineCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

   <%-- Write Server code in Script Tag with runat attribute--%>
    <script runat="server">

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Hello Asp.net");

            this.FullName = "Kishor Naik";

            // Code Behind level Lable Control Binding
            lblFullNameServer.Text = Server.HtmlEncode( this.FullName);

            Page.DataBind();

        }

        public string FullName { get; set; }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
              <%--  Display Expression Page Level Binding--%>
            <span>
            <%: Server.HtmlEncode(this.FullName) %>
            </span>

          <%--  Binding Expression Page Level Binding--%>
           <asp:Label runat="server" ID="lblFullName" Text="<%# Server.HtmlEncode( this.FullName) %>"></asp:Label>

        <asp:Label runat="server" ID="lblFullNameServer"></asp:Label>

            
    </div>
    </form>
</body>
</html>
